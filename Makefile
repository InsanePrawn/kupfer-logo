.PHONY: all clean
inkscape_args := --export-type=svg -l template.svg
bg_fill_sed := sed -e 's/"display:none"/"display:inline"/g'

all: kupfer-white-transparent.svg kupfer-black-transparent.svg kupfer-white-filled.svg kupfer-black-filled.svg

clean:
	rm -f kupfer-*.svg

kupfer-white-transparent.svg: template.svg Makefile
	inkscape $(inkscape_args) -o $@

kupfer-black-transparent.svg: kupfer-white-transparent.svg
	sed -e 's/;fill:#ffffff;/;fill:#000000;/g' -e 's/;stroke:#000000;/;stroke:#ffffff;/g' -e 's/fill:#343a40;/fill:#ffffff;/g' kupfer-white-transparent.svg > $@

kupfer-white-filled.svg: kupfer-white-transparent.svg
	$(bg_fill_sed) kupfer-white-transparent.svg > $@

kupfer-black-filled.svg: kupfer-black-transparent.svg
	$(bg_fill_sed) kupfer-black-transparent.svg > $@
